

class Logger(object):
    """ Logger is an auxiliary class for granular logging

        Args:
            name (str): Class that invokes Logger instance

        Attributes:
            _name: name of the Logger instance
            _level: debug mode, default is 'DEBUG'
        """

    def __init__(self, name, level="DEBUG"):
        self._name = name
        self._level = level

    def debug(self, function, msg):
        """ DEBUG log

        :param function: function that calls log
        :param msg: msg to log
        """
        self._log("DEBUG", function, msg)

    def info(self, function, msg):
        """ INFO log

        :param function: function that calls log
        :param msg: msg to log
        """
        self._log("INFO", function, msg)

    def error(self, function, msg):
        """ ERROR log

        :param function: function that calls log
        :param msg: msg to log
        """
        self._log("ERROR", function, msg)

    def _log(self, function_log_level, function, msg):
        """ logs according level set

        :param function_log_level: log level
        :param function: function that calls log
        :param msg: msg to log
        """
        if self.levelValue(self._level) >= self.levelValue(function_log_level):
            print ("%s || %s || %s || %s" % (function_log_level, self._name, function, msg))

    def levelValue(self, log_level):
        """ Validates if current level is available to log

        :param log_level:
        :return: Integer for comparison
        """
        return 3 if log_level is "DEBUG" else (2 if log_level is "INFO" else 1)

