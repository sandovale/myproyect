import requests
import urllib3
import json
from rs.commons.Logger import Logger


class UmaClient:
    """ UmaOxTrustClient brings CRUD operations to oxTrust REST API authenticating a client through UMA protocol

    Args:
        hostname (str): Server hostname. eg: gluu.example.com
        credentials (str): client_id:client_secret in Base64 format. Can be encoded here -> https://www.base64encode.org/
        endpoint (str): OxTrust API endpoint for operation, without '/api/v1/' prefix. https://gluu.org/docs/oxtrust-api/4.0/

    Attributes:
        _operation_endpoint (str): Api endpoint instantiated with hostname and endpoint Args
        _token_endpoint (str): Token endpoint instantiated with hostname (for getting UMA RPT token)
        _basic_header (str): Header to get RPT with client credentials in Base64 when token_endpoint is called
        _RPT (str): Variable that stores the last RPT token requested to token_endpoint
    """

    def __init__(self, hostname, credentials, endpoint):
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
        self._logger = Logger("UmaClient")
        self._operation_endpoint = "https://%s/identity/restv1/api/v1/%s" % (hostname, endpoint)
        self._token_endpoint = "https://%s/oxauth/restv1/token" % hostname
        self._basic_header = {
            'Authorization': "Basic %s" % credentials,
            'Cache-Control': "no-cache",
            'Content-Type': "application/x-www-form-urlencoded",
            'Connection': "keep-alive",
            'cache-control': "no-cache"
        }
        self._RPT = ""

    def create(self, json_str):
        """ Creates an object calling _operations_endpoint via REST

        :param json_str: String that represents an object to be created
        :return:
            Success > String that represents object created
            Fail > None
        """
        self._logger.debug("create", "CREATE with JSON: %s" % json_str)
        return self._execute('POST', json_str)

    def delete(self, json_str):
        """ Deletes a single object calling _operations_endpoint via REST

        :param json_str: String with a json which has an inum attr value to be obtained
        :return:
            Success > String with inum value from the deleted object
            Fail > None
        """
        json_obj = json.loads(json_str)
        inum = json_obj.get("inum", None)
        self._logger.debug("delete", "DELETE with inum: %s" % inum)
        return self._execute('DELETE', "", inum)

    def get(self, json_str):
        """ Gets a single object calling _operations_endpoint via REST

        :param json_str: String with a json which has an inum attr value to be obtained
        :return:
            Success > String that represents object founded
            Fail > None
        """
        json_obj = json.loads(json_str)
        inum = json_obj.get("inum", None)
        self._logger.debug("get", "GET with inum: %s" % inum)
        return self._execute('GET', "", inum)

    def update(self, json_str):
        """ Updates an object calling _operations_endpoint via REST

        :param json_str: String that represents an object to be Updated. It must have an inum field in JSON
        :return:
            Success > String that represents object updated
            Fail > None
        """
        self._logger.debug("put", "PUT with JSON: %s" % json_str)
        return self._execute('PUT', json_str)

    def upsert(self, json_str):
        """Creates an object if does not exist otherwise updates it

        :param json_str: String that represents an object to be Created/Updated
        :return:
            Success > String that represents object created/updated
            Fail > None
        """
        self._logger.debug("upsert", "UPSERT with JSON: %s" % json_str)
        return self.update(json_str) if 'inum' in json.loads(json_str) else self.create(json_str)

    def _execute(self, operation, json_str, inum=""):
        """Executes a HTTP requests to oxTrust API with UMA protocol

        :param operation: POST/PUT/GET/DELETE
        :param json_str: String with json format. Body of the HTTP request.
        :param inum: Optional for DELETE/GET operations. Default ""
        :return:
            Success > json string with the body of the message
            Fail > None
        """
        function = "_execute"
        endpoint_url = "%s/%s" % (self._operation_endpoint, inum)
        response = self._getResponse(operation, endpoint_url, json_str)
        if response.status_code == 401:
            self._logger.debug(function, "RPT is invalid/expired, Getting a new one...")
            self._RPT = self._getRPT(operation)
            self._logger.debug(function, "Trying operation with a new RPT - %s" % self._RPT)
            response = self._getResponse(operation, endpoint_url, json_str)

        if response.status_code is not 200 or not self._isJson(response.content.decode()):
            self._logger.error(function, "Returning None - HTTP code: %d - HTTP content: %s" % (
                response.status_code, response.content))
            return None

        return {"inum": inum} if operation is 'DELETE' else response.content

    def _isJson(self, my_json):
        """Verify if a String is a Json representation

        :param my_json: String to verify
        :return: boolean
        """
        if my_json is not "":
            try:
                json.loads(my_json)
            except ValueError as e:
                return False
        return True

    def _getResponse(self, operation, endpoint_url, json_str):
        """Executes a single request against oxTrust API

        :param operation: POST/PUT/GET/DELETE
        :param endpoint_url: REST endpoint for HTTP request
        :param json_str: String with json format. Body of the HTTP request.
        :return: a Response object based on requests library
        """
        return requests.request(operation, endpoint_url, data=json_str, headers=self._getOperationHeaders(self._RPT),
                                verify=False)

    def _getRPT(self, operation):
        """Gets RPT token for UMA authentication

        :param operation: POST/PUT/GET/DELETE
        :return: String that represents the token to be used for UMA calls against oxTrust API
        """
        function = "getRPT"
        ticket = ""
        acc_token = ""
        self._logger.info(function, "starting RPT getting process")

        response = requests.request(operation, self._operation_endpoint, data="", headers=self._getOperationHeaders(""),
                                    verify=False)
        if response.status_code == 401:
            www_authenticate_headers = response.headers['WWW-Authenticate']
            ticket = dict(x.split("=") for x in www_authenticate_headers.split(",")).get(' ticket')
            self._logger.debug(function, "Ticket value is %s" % ticket)
        else:
            self._logger.error(function, "failed get RPT")

        payload = "grant_type=urn:ietf:params:oauth:grant-type:uma-ticket&ticket=%s" % ticket
        response = requests.request("POST", self._token_endpoint, data=payload, headers=self._basic_header,
                                    verify=False)
        if response.status_code != 200:
            self._logger.error(function, "failed get access_token")
        else:
            json_response = json.loads(response.text)
            acc_token = json_response['access_token']
            self._logger.info(function, "access_token is %s" % acc_token)
        return acc_token

    def _getOperationHeaders(self, ticket):
        """Builds HTTP Header for requests to _operation_endpoint

        :param ticket: RPT token value
        :return: a dict to be used as a 'headers' parameter on a request object
        """
        return {
            'Authorization': "Bearer %s" % ticket,
            'Content-Type': "application/json",
            'Connection': "keep-alive",
            'cache-control': "no-cache"
        }
