import json
from os import listdir
from datetime import datetime
from os.path import isfile, join, isdir
from rs.commons.Logger import Logger
from rs.oxtrust.UmaClient import UmaClient


class DeploymentClient:
    """ Deployment client brings functionality to execute CREATE/DELETE/UPDATE/GET/UPSERT getting objects from a file or a folder with multiple json files.
        json file can be a object or an array. json files extension must be '.json'

        Args:
            hostname (str): Server hostname. eg: gluu.example.com
            credentials (str): client_id:client_secret in Base64 format. Can be encoded here -> https://www.base64encode.org/
            endpoint (str): OxTrust API endpoint for operation, without '/api/v1/' prefix. https://gluu.org/docs/oxtrust-api/4.0/

        Attributes:
            _uma_client: client to Gluu instance to do operations
        """

    FOLDER = "FOLDER"
    FILE = "FILE"

    def __init__(self, hostname, credentials, endpoint):
        self._logger = Logger("BulkClient")
        self._uma_client = UmaClient(hostname, credentials, endpoint)

    def create(self, file_path):
        """ Do named operation with a file/folder which has json files

        :param file_path: path to the file/folder with json files
        """
        self._bulk_action(self._uma_client.create, file_path)

    def delete(self, file_path):
        """ Do named operation with a file/folder which has json files

        :param file_path: path to the file/folder with json files
        """
        self._bulk_action(self._uma_client.delete, file_path)

    def get(self, file_path):
        """ Do named operation with a file/folder which has json files

        :param file_path: path to the file/folder with json files
        """
        self._bulk_action(self._uma_client.get, file_path)

    def update(self, file_path):
        """ Do named operation with a file/folder which has json files

        :param file_path: path to the file/folder with json files
        """
        self._bulk_action(self._uma_client.update, file_path)

    def upsert(self, file_path):
        """ Do named operation with a file/folder which has json files

        :param file_path: path to the file/folder with json files
        """
        self._bulk_action(self._uma_client.upsert, file_path)

    def _bulk_action(self, fun, file_path):
        """ Do named operation with a file/folder which has json files. function is a parameter

        :param fun: function to execute from a uma_client object
        :param file_path: path to the file/folder with json files
        """
        function = "_bulk_action"
        json_array = self._load_bulk_json(file_path)
        ok = 0
        failed = 0
        time = datetime.now().strftime('%Y%m%d-%H.%M.%S.%f')[:-3]
        file_ok = open("OK-%s.log" % time , "w")
        file_failed = open("FAIL-%s.log" % time, "w")
        total = len(json_array)
        for json_obj in json_array:
            try:
                json_str = json.dumps(json_obj)
                result = fun(json_str)
                if result is None:
                    failed += 1
                    file_failed.write(json_str + '\n')
                    self._logger.error(function, 'FAILED - %s' % json_str)
                else:
                    ok += 1
                    file_ok.write(result.decode() + '\n')
                    self._logger.info(function, 'OK - %s' % json_str)
            except:
                failed += 1
                file_failed.write(json_str + '\n')
                self._logger.error("FAILED - %s" % json_str)
        print ("""
        ============================================================
        - DEPLOY OPERATION FINISHED -
        OK: %d
        FAILED: %d
        TOTAL: %d
        ============================================================
        Detail about created/failed objects in:
        Created: %s
        Failed: %s
        ============================================================
        """ % (ok, failed, total, file_ok.name, file_failed.name))
        file_ok.close()
        file_failed.close()

    def _load_bulk_json(self, path_to_load):
        """ Loads JSON from a file in a variable, also validates the JSON

        :param path_to_load: path to the json file
        :return:
        """
        files_path = [path_to_load] if not isdir(path_to_load) else ["%s/%s" % (path_to_load, f) for f in listdir(path_to_load) if isfile(join(path_to_load, f)) and f.endswith(".json")]
        json_to_process = []
        for file_path in files_path:
            with open(file_path) as f:
                try:
                    data = f.read().replace('\n', '')
                    val = json.loads(data)
                    if isinstance(val, list):
                        json_to_process += val
                    elif isinstance(val, dict):
                        json_to_process.append(val)
                    else:
                        raise Exception('File content is not a JSON obj/array')
                except ValueError:
                    raise Exception('File content is not a JSON obj/array')
        return json_to_process
