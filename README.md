# python-commons

Python packages and scripts commons on RoundServices projects

## Setup

### Pre-requisites
- Python > 3.2.1
- PIP (package-management system)

## Deploy
To install/upgrade Round Services &copy; python-commons library, execute the following command on your server

- SSH deploy
```sh
pip install --upgrade git+ssh://git@gitlab.com/roundservices/python-commons.git@master
```
- HTTPS deploy
```sh
pip install --upgrade git+https://GITLAB_USER@gitlab.com/roundservices/python-commons.git@master
```
  - replace `GITLAB_USER` with your GitLab account name (it will require account password)

## Coding

All classes contained in **rs** folder can be used for develop awesome scripts or classes, just import the dependencies on your python code:
```python
from rs.package.MyClass import MyClass
```
