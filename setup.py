from setuptools import setup

setup(
    name='python-commons',
    version='1.0.0',
    description='Python packages and scripts commons on RoundServices projects',
    url='git@gitlab.com:roundservices/python-commons.git',
    author='Round Services',
    author_email='sandovalezequiel@roundservices.biz',
    license='MIT License',
    install_requires=['requests', 'urllib3'],
    packages=['rs', 'rs.commons', 'rs.oxtrust'],
    zip_safe=False,
    python_requires='>=2.7'
)
