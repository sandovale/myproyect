#!/usr/bin/env python

"""
=========================================================================================
== OxTrust Deployer                                                                    ==
== Script which deploys json objects (from a file or folder) throught oxTrust REST API.==
== After execution it creates OK and FAIL files with processed objects                 ==
=========================================================================================
"""

from __future__ import print_function
import sys
import argparse
from rs.oxtrust.DeploymentClient import DeploymentClient


def main(arguments):

    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('hostname', help="Gluu hostname - e.g. -> gluu.mydomain.com")
    parser.add_argument('credentials', help="client_id:client_secret encoded with Base64")
    parser.add_argument('operation', help="Can be CREATE/UPDATE/DELETE/GET/UPSERT")
    parser.add_argument('rest_endpoint', help="operational endpoint from https://gluu.org/docs/oxtrust-api/4.0/ without '/api/v1/' prefix")
    parser.add_argument('objects_path', help="path to json file or folder that contains json files (.json extension is mandatory)")
    args = parser.parse_args(arguments)

    operation = args.operation.upper()
    objects_path = args.objects_path
    client = DeploymentClient(args.hostname, args.credentials, args.rest_endpoint)

    if operation == 'CREATE':
        client.create(objects_path)
    elif operation == 'UPDATE':
        client.update(objects_path)
    elif operation == 'DELETE':
        client.delete(objects_path)
    elif operation == 'GET':
        client.get(objects_path)
    elif operation == 'UPSERT':
        client.upsert(objects_path)
    else:
        print("OxTrust Deployer - Operation required does not exist")


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
